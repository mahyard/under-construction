# under-construction


## Usage
Clone this repo to `/var/www/html` directory and create a vhost like this:

### Apache

```
<VirtualHost *:80>
    ServerName example.com
    DocumentRoot /var/www/html
    <Directory /var/www/html>
        DirectoryIndex under_construction.html
    </Directory>
</VirtualHost>
```

### Nginx
```
server {
  listen 80;
  server_name example.com;
  root /var/www/html;
  index under_construction.html;
  location / {
    try_files /under_construction.html =404;
  }
}
```
